import { Module } from '@nestjs/common';
import { BooksService } from './books.service';
import { BooksController } from './books.controller';
import { PrismaModule } from "../prisma/prisma.module";
import { GenresModule } from "../genres/genres.module";

@Module({
  controllers: [BooksController],
  providers: [BooksService],
  imports: [PrismaModule, GenresModule]
})
export class BooksModule {}
