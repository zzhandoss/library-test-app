export const successResponse = (payload: Record<string, any>) => {
  return {
    success: true,
    data: payload
  }
}

export const responseWithError = (message: string) => {
  return {
    success: false,
    message: message
  }
}