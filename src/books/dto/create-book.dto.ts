import { IsArray, IsDate, IsNotEmpty, IsNumber, IsString, Max, Min } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateBookDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  title: string

  @IsNumber()
  @Min(0)
  @Max(2024)
  @IsNotEmpty()
  @ApiProperty()
  publishedAt: number

  @IsNotEmpty()
  @IsArray()
  @ApiProperty()
  genres: number[]

  @IsNotEmpty()
  @IsString()
  @ApiProperty()
  author: string
}