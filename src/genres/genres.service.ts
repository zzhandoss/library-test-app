import { Injectable } from '@nestjs/common';
import { PrismaService } from "../prisma/prisma.service";

@Injectable()
export class GenresService {
  constructor(private prisma: PrismaService) {}

  async getAllGenres() {
    return await this.prisma.genre.findMany()
  }

  async getAllGenreIds() {
    return (await this.prisma.genre.findMany()).map((genre) => genre.id)
  }
}
