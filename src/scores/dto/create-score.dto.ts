import { IsNotEmpty, IsNumber, Max, Min } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateScoreDto {
  @IsNumber()
  @Min(1)
  @Max(10)
  @IsNotEmpty()
  @ApiProperty()
  score: number

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  bookId: number

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  userId: number
}
