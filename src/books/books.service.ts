import { Injectable } from '@nestjs/common';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import { GetBooksDto } from "./dto/get-books.dto";
import { PrismaService } from "../prisma/prisma.service";
import { GenresService } from "../genres/genres.service";
import { responseWithError, successResponse } from "../../shared/responses";

@Injectable()
export class BooksService {
  constructor(
    private prisma: PrismaService,
    private genresService: GenresService,
    ) {}
  // Желательно создать классы репозиториев, а для других доменных единиц подключать соответсвующее сервисы (я не делал)
  async create(createBookDto: CreateBookDto) {
    console.log(createBookDto);
    const genres = await this.genresService.getAllGenreIds()
    if (createBookDto.genres.some((genre) => !genres.includes(genre))) {
      return responseWithError("Несоответствие жанров")
    }
    // => Author service
    let authorId: number
    const existedAuthor = await this.prisma.author.findFirst({
      where: {
        name: createBookDto.author
      }
    })
    if (!existedAuthor) {
      const newAuthor = await this.prisma.author.create({
        data: {
          name: createBookDto.author
        }
      })
      authorId = newAuthor.id
    } else {
      authorId = existedAuthor.id
    }

    const createdBook = await this.prisma.book.create({
      data: {
        title: createBookDto.title,
        authorId: authorId,
        publishedAt: createBookDto.publishedAt,
      },
      include: {
        author: true,
        genres: true,
      }
    })
    const genreAssociations = createBookDto.genres.map((genreId: number) => ({
      bookId: createdBook.id,
      genreId: genreId,
    }))
    await this.prisma.bookGenres.createMany({
      data: genreAssociations
    })

    return successResponse(createdBook)
  }

  async findAll(getBooksQueryDto: GetBooksDto) {
    console.log(getBooksQueryDto)
    const TAKE_PER_PAGE = 5
    let booksByAggregateScore
    if (getBooksQueryDto) {
      booksByAggregateScore = await this.prisma.bookScores.groupBy({
        by: ['bookId'],
        _avg: {
          score: true,
        },
        orderBy: {
          _avg: {
            score: getBooksQueryDto.orderByScore === "DESC" ? "desc" : "asc",
          },
        },
      });
    }
    let books = await this.prisma.book.findMany({
      where: {
        title: getBooksQueryDto.title ? { contains: getBooksQueryDto.title, mode: "insensitive" } : undefined,
        author: getBooksQueryDto.author ? { name: { contains: getBooksQueryDto.author, mode: "insensitive" } } : undefined,
        genres: getBooksQueryDto.genres?.length > 0 ? { some: { genreId: { in: getBooksQueryDto.genres } } } : undefined,
        publishedAt: getBooksQueryDto.publishedAt ?  getBooksQueryDto.publishedAt : undefined,
      },
      include: {
        author: true,
        genres: true,
      },
      skip: getBooksQueryDto.page ? (getBooksQueryDto.page - 1) * TAKE_PER_PAGE : undefined,
      take: getBooksQueryDto.page ? TAKE_PER_PAGE : undefined,
    })

    if (getBooksQueryDto.orderByScore) {
      const booksWithScores = books.map((book) => ({...book, score: booksByAggregateScore.find((b) => b.bookId === book.id)?._avg.score ?? 0 }))
      booksWithScores.sort((a ,b) => getBooksQueryDto.orderByScore === "DESC" ?  b.score - a.score : a.score - b.score)
      books = booksWithScores
    }

    return successResponse(books)
  }

  findOne(id: number) {
    return `This action returns a #${id} book`;
  }

  async update(id: number, updateBookDto: UpdateBookDto) {
    const updateBook = await this.prisma.book.update({
      where: {
        id
      },
      data: {
        title: updateBookDto.title,
        publishedAt: updateBookDto.publishedAt,
      }
    })
    if (updateBookDto.author) {
      const isAuthorExists = await this.prisma.author.findFirst({
        where: {
          name: updateBookDto.author
        }
      })
      let authorId = isAuthorExists?.id
      if (!isAuthorExists) {
        const newAuthor = await this.prisma.author.create({
          data: {
            name: updateBookDto.author,
          }
        })
        authorId = newAuthor.id
      }
      await this.prisma.book.update({
        where: {
          id
        },
        data: {
          authorId: authorId
        }
      })
    }
    if (updateBookDto.genres) {
      const booksGenres = await this.prisma.bookGenres.findMany({
        where: {
          bookId: id
        }
      })
      const alteredBooksGenres = booksGenres.map((genre) => genre.genreId)
      for (let updatedGenre of updateBookDto.genres) {
        const index = alteredBooksGenres.indexOf(updatedGenre);
        if (index !== -1) {
          alteredBooksGenres.splice(index, 1);
        } else {
          await this.prisma.bookGenres.create({
            data: {
              bookId: id,
              genreId: updatedGenre
            }
          })
        }
      }
      await this.prisma.bookGenres.deleteMany({
        where: {
          genreId: {
            in: alteredBooksGenres
          }
        }
      })

    }
    return successResponse(updateBook)
  }

  async remove(id: number) {
    await this.prisma.book.update({
      where: {
        id
      },
      data: {
        genres: {
          deleteMany: {
            bookId: id
          }
        },
        scores: {
          deleteMany: {
            bookId: id
          }
        }
      }
    })
    await this.prisma.book.delete({
      where: {
        id
      },
    })
    return successResponse({message: "Успешно удалено"})
  }

  async rateBook(score: number, bookId: number, userId: number) {
    const rating = await this.prisma.bookScores.create({
      data: {
        score: score,
        bookId: bookId,
        userId: userId
      }
    })

    return successResponse(rating)
  }
}
