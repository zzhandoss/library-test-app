import { Controller, Get, Post, Body, Patch, Param, Delete, Query } from "@nestjs/common";
import { BooksService } from './books.service';
import { CreateBookDto } from './dto/create-book.dto';
import { UpdateBookDto } from './dto/update-book.dto';
import {GetBooksDto, GetBooksDTOTransformPipe} from "./dto/get-books.dto";

@Controller('books')
export class BooksController {
  constructor(private readonly booksService: BooksService) {}

  @Post()
  create(@Body() createBookDto: CreateBookDto) {
    return this.booksService.create(createBookDto);
  }

  @Get()
  findAll(@Query(new GetBooksDTOTransformPipe()) getBooksQueryDto: GetBooksDto) {
    return this.booksService.findAll(getBooksQueryDto);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.booksService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateBookDto: UpdateBookDto) {
    return this.booksService.update(+id, updateBookDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.booksService.remove(+id);
  }

  @Post("rate")
  // Здесь должен быть декоратор на проверку авторизации, и через другой декоратор можно достать юзера и соответственно его id
  rateBook(@Body() { bookId, score } : { bookId: number, score: number }) {
    const userId = 1
    return this.booksService.rateBook(score, bookId, userId)
  }
}
