import { IsArray, IsIn, IsNotEmpty, IsNumber, IsOptional, IsString, Max, Min } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";
import {ArgumentMetadata, Injectable, PipeTransform} from "@nestjs/common";
import {plainToInstance, Type} from "class-transformer";

export class GetBooksDto {
  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  title?: string

  @IsString()
  @IsOptional()
  @ApiProperty({ required: false })
  author?: string

  @IsArray()
  @IsNumber({}, {each: true})
  @IsOptional()
  @ApiProperty({ required: false })
  @Type(() => Number)
  genres?: number[]

  @IsNumber()
  @Min(0)
  @Max(2025)
  @IsOptional()
  @Type(() => Number)
  publishedAt?: number

  @IsOptional()
  @IsNumber()
  @Type(() => Number)
  @Min(1)
  page?: number

  @IsString()
  @IsOptional()
  @IsNotEmpty()
  @IsIn(["ASC", "DESC"])
  orderByScore?: "ASC" | "DESC"
}

@Injectable()
export class GetBooksDTOTransformPipe implements PipeTransform {
  async transform(getBooksDto: GetBooksDto, { metatype }: ArgumentMetadata) {
    if (!metatype) {
      return getBooksDto
    }

    return plainToInstance(metatype, getBooksDto)
  }
}