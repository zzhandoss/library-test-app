import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

const main = async () => {
  const [defaultUser, ...defaultGenres] = await Promise.all([
    prisma.user.upsert(newUserSeed("admin@example.com", "admin123Q")),
    prisma.genre.upsert(newGenreSeed("fiction")),
    prisma.genre.upsert(newGenreSeed("non-fiction")),
    prisma.genre.upsert(newGenreSeed("action")),
    prisma.genre.upsert(newGenreSeed("romance")),
    prisma.genre.upsert(newGenreSeed("drama")),
  ])

  console.log("Created default user, ", defaultUser);
  console.log("Available genres: ", defaultGenres)
}

const newUserSeed = (email: string, password: string) => (
  {
    where: {
      email: email
    },
    create: {
      email: email,
      password: password
    },
    update: {},
  }
)

const newGenreSeed = (name: string) => (
  {
    where: { name },
    create: { name },
    update: {},
  }
)

main().catch(e => {
  console.error(e);
  process.exit(1)
}).finally(async () => {
  await prisma.$disconnect()
})